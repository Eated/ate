import greenfoot.*;

/**
 * Write a description of class Wallpaper here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Wallpaper extends World
{

    /**
     * Constructor for objects of class Wallpaper.
     * 
     */
    int state = 0;
    int MENU = 0;
    int PLAYING = 1;
    int level = 1;
    Title title = new Title();
    Paddle paddle = new Paddle();
    int abc=0;
    Pressspace pressspace = new Pressspace();
    public Wallpaper()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1,false); 
        addObject(title, getWidth()/2, getHeight()/2);
        addObject(pressspace , getWidth()/2, getHeight()/8*7);
    }

    public void act() {
        if (state == MENU) {
            if (Greenfoot.isKeyDown("space")) {
                state = 1;
            }
        }
        else if (state == PLAYING) {
            if (level == 1) {
                Brick brick = new Brick();
                for (int u = 0; u < 5; u++){
                    for (int s = -7; s < 7; s++){
                        addObject(new Brick(), getWidth()/2 - (s  * brick.getImage().getWidth())-brick.getImage().getWidth()/2, getHeight()/15 + brick.getImage().getHeight() * u); 
                    }
                }
                addObject(paddle, getWidth()/2, getHeight() - getHeight()/25);
                Ball ball = new Ball();
                addObject(ball, getWidth()/3,getHeight()- getHeight()/4);
                ball.setLocation(ball.getX(),ball.getY() - ball.getImage().getWidth()*3/2);
                level = 2;
                removeObject(title);
            }
            if(level ==2){
                abc++;
                if(abc>=30){
                    if (Greenfoot.isKeyDown("space")) {
                        removeObject(pressspace);
                    }
                }
            }
        }
    }
}
