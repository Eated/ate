import greenfoot.*;
import java.awt.Color;
/**
 * Write a description of class pressspace here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Pressspace extends Actor
{
    int state = 0;
    public Pressspace (){
        if(state == 0){
            setImage(new GreenfootImage("Press Space to Start" , 20, Color.RED,null));
            if (Greenfoot.isKeyDown("space")) {
                state = 1;
            }
        }else if(state == 1){
            setImage(new GreenfootImage("Press Space to Begin" , 20, Color.GREEN,null));
        }
    }

}
