import greenfoot.*;

/**
 * Write a description of class Ball here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Ball extends Actor
{
    int dx = 3;
    int dy = 3;
    int w = getImage().getWidth()/2;
    int state = 0;
    int abc = 0;
    /**
     * 
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {   
        abc++;
        if (Greenfoot.isKeyDown("space")&&abc >=30) {
                state = 1;
        }
        if(state == 1){
            setLocation(getX()+dx,getY()+dy);
            int WH = getWorld().getHeight();
            int WW = getWorld().getWidth();
            int tenW = WW/9;
            int tenH = WH/9;
            if(getX()-w<=0){
                dx *= -1;
            }
            if(getY()-w<=0){
                dy *= -1;
            }
            if(getX()+w>=WW){
                dx *= -1;
            }
            if(getY()+w>=WH){
                dy *= -1;
            }
            Actor brick = getOneIntersectingObject(Brick.class);
            if(brick != null){
                int brickX = brick.getX();
                int brickY = brick.getY();
                if((brickX + brick.getImage().getWidth()/2 >= getX() - getImage().getWidth()/2 )&&(getX()>=brickX + brick.getImage().getWidth()/2)){//right edge brick, left edge ball && center ball, right edge
                    dx *= -1;
                }
                if((brickX - brick.getImage().getWidth()/2 <= getX() + getImage().getWidth()/2)&&(getX()<=brickX - brick.getImage().getWidth()/2)){//left edge brick, right edge ball
                    dx *= -1;
                }
                if((brickY + brick.getImage().getHeight()/2 >= getY() - getImage().getHeight()/2)&&(brickY + brick.getImage().getHeight()/2 <=getY())){//bottom of brick, top of ball
                    dy *= -1;
                    
                }
                if((brickY - brick.getImage().getHeight()/2 <= getY() + getImage().getHeight()/2)&&(brickY - brick.getImage().getHeight()/2 >=getY())){//top of brick, bottom of ball
                    dy *= -1;
                }
                removeTouching(Brick.class);
            }
            if(isTouching(Paddle.class)){
                if(dy >=0){
                    dy *= -1;
                }
            }
        }
    }    
}
